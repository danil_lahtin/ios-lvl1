//
//  Menu.swift
//  iOS lvl1
//
//  Created by Danil Lahtin on 21/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import Foundation

class Menu: NSObject {
    static var items = [MenuItem]()
    
    override init() {
        super.init()
    }
    
    static func addMenuItem(withName name: String, andPage page: Int){
        if page >= 0 {
            self.items.append(MenuItem(title: name, page: page))
        }
    }
}

struct MenuItem {
    var title = "Default title"
    var page = 0
}