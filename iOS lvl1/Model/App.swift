//
//  App.swift
//  iOS lvl1
//
//  Created by Danil Lahtin on 21/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import UIKit

class App: NSObject {
    
    // MARK: - Fields
    var name = "Default Name"
    var version = "Default Version"
    var time = Double()
    var url = "Default imageURL"
    var file = "Default fileURL"
    var comment = ""
    
    
    // MARK: - Methods
    
    override var description: String{
        return "name: \(self.name)\n\r" +
            "version: \(self.version)\n\r" +
            "time: \(self.time)\n\r" +
            "url: \(self.url)\n\r" +
            "file: \(self.file)\n\r" +
            "comment: \(self.comment)\n\r"
    }
    
    override init() {
        super.init()
    }
    
    init(withName name: String, version: String, timeInterval: Double, imageURL: String, fileURL: String, comment: String){
        super.init()
        self.name = name
        self.version = version
        self.time = timeInterval
        self.url = imageURL
        self.file = fileURL
        self.comment = comment
    }
    
    // Creates sample array of Apps. Just to check class work
    static func sampleAppsArray() -> [App]{
        var apps = [App]()
        
        apps.append(App())
        apps.append(App())
        apps.append(App())
        apps.append(App())
        apps.append(App())
        
        return apps
    }
}
