//
//  MenuTableViewController.swift
//  iOS lvl1
//
//  Created by Danil Lahtin on 22/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import UIKit

protocol MenuTableViewControllerDelegate {
    func didSelectMenuItem(atIndexPath indexPath: NSIndexPath)
}

class MenuTableViewController: UITableViewController {

    var delegate: MenuTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Menu.items.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath)

        cell.textLabel?.text = Menu.items[indexPath.row].title

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.delegate?.didSelectMenuItem(atIndexPath: indexPath)
    }

}
