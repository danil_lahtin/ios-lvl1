//
//  ListTableViewCell.swift
//  iOS lvl1
//
//  Created by Danil Lahtin on 21/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    var app: App!{
        didSet{
            setupThumbnailImage()
            self.titleLabel.text = self.app.name
            self.versionLabel.text = self.app.version
            self.dateLabel.text = "\(self.app.time.timeIntervalToString())"
        }
    }
    
    // MARK: - Outlets
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    // MARK: - UITableViewCell
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func setupThumbnailImage(){
        let s = "http://orig13.deviantart.net/6d4c/f/2013/341/4/a/ios_7_launchpad_icon_for_mac_by_djtech42-d6x5h94.png"
        
        APIManager.sharedInstance.downloadImage(fromURL: s){ (image, success) in
            dispatch_async(dispatch_get_main_queue()){
                if success {
                    self.thumbnailImageView.image = image
                } else {
                    self.thumbnailImageView.image = UIImage(named: "noImage")
                }
            }
        }
    }
    

}
