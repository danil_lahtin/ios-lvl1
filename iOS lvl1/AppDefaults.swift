//
//  AppDefaults.swift
//  iOS lvl1
//
//  Created by Danil Lahtin on 21/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import Foundation
import UIKit

struct AppDefaults {
    
    static let defaultURLString = "http://bnet.i-partner.ru/projects/playMarket/?json=1"
    static let defaultTimeoutInterval = 10.0
    
    static var mainTabBarController: UITabBarController?
    
    static let defaultMaskColor = UIColor(white: 0, alpha: 0.5)
    static let defaultAnimationTime = 0.6
    static let defaultMenuWidth = CGFloat(0.75)
    
    static let defaultDateFormat = "yyyy-MM-dd HH:mm:ss"
}