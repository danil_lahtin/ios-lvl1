//
//  HTTPClient.swift
//  iOS lvl1
//
//  Created by Danil Lahtin on 21/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import UIKit

class HTTPClient: NSObject {
    
    func createURLRequest(withURL urlString: String = AppDefaults.defaultURLString, httpMethod method: String = "POST", params: [String:AnyObject]? = nil, headers: [String:String]? = nil) -> NSMutableURLRequest{
        
        // Setup NSURL
        var url: NSURL? = NSURL(string: urlString)
        
        if url == nil {
            url = NSURL(string: AppDefaults.defaultURLString)
        }
        
        // Setup request
        let request = NSMutableURLRequest(URL: url!, cachePolicy: .ReloadIgnoringLocalCacheData, timeoutInterval: AppDefaults.defaultTimeoutInterval)
        
        // Set request method
        request.HTTPMethod = method
        
        
        // Build parameters string to set HTTPBody
        var paramsString = "?"
        
        if let params = params {
            for (key,value) in params{
                paramsString += "\(key)=\(value)&"
            }
        }
        paramsString.removeAtIndex(paramsString.endIndex.predecessor())
        request.HTTPBody = paramsString.dataUsingEncoding(NSUTF8StringEncoding)
        
        
        // Build headers
        if headers != nil{
            for header in headers!{
                request.addValue(header.1, forHTTPHeaderField: header.0)
            }
        }
        
        return request
    }
    
    private func dataTask(withRequest request: NSURLRequest, completionHandler: (AnyObject?, NSURLResponse?, NSError?)->()){
        
        NSURLSession.sharedSession().dataTaskWithRequest(request){ (data, response, error) in
            if error != nil {
                completionHandler(nil, nil, error)
                return
            }
            if let response = response as? NSHTTPURLResponse where 200...299 ~= response.statusCode{
                completionHandler(data, nil, nil)
            } else {
                completionHandler(data, response, nil)
                return
            }
            }.resume()
    }
    
    private func dataTask(withURL urlString: String, completionHandler: (AnyObject?, NSURLResponse?, NSError?)->()){
        
        var url: NSURL? = NSURL(string: urlString)
        
        if url == nil {
            url = NSURL(string: AppDefaults.defaultURLString)
        }
        
        NSURLSession.sharedSession().dataTaskWithURL(url!){ (data, response, error) in
            if error != nil {
                completionHandler(nil, nil, error)
                return
            }
            if let response = response as? NSHTTPURLResponse where 200...299 ~= response.statusCode{
                completionHandler(data, nil, nil)
            } else {
                completionHandler(data, response, nil)
                return
            }
            }.resume()
    }
    
    func getJSON(request: NSURLRequest, completionHandler: (AnyObject?, NSError?) -> ()){
        dataTask(withRequest: request){(data, response, error) in
            if (error != nil){
                print(error!.localizedDescription)
                completionHandler(nil, error)
                return
            }
            if (response != nil){
                print(response)
                completionHandler(nil, nil)
                return
            }
            
            do {
                let json = try (NSJSONSerialization.JSONObjectWithData(data! as! NSData, options: .MutableContainers))
                completionHandler(json, nil)
            } catch let jsonError as NSError{
                completionHandler(nil, jsonError)
                return
            }
            
        }
    }
    
    func getImage(withURL urlString: String, completionHandler: (UIImage?, Bool?)->()){
        dataTask(withURL: urlString){(data, response, error) in
            if (error != nil){
                print(error!.localizedDescription)
                completionHandler(nil, false)
                return
            }
            if (response != nil){
                print(response)
                completionHandler(nil, false)
                return
            }
            if let imageData = data as? NSData{
                let image = UIImage(data: imageData)
                completionHandler(image, true)
            } else {
                completionHandler(nil, false)
                return
            }
        }
    }
}
