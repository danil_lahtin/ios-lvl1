//
//  DataManager.swift
//  iOS lvl1
//
//  Created by Danil Lahtin on 21/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import UIKit

class DataManager<T>: NSObject {
    
    // MARK: - Fields
    var items = [T]()
    
    
    // MARK: - Methods
    func getItem(atIndex index: Int) -> T?{
        if 0..<self.items.count ~= index {
            return self.items[index]
        } else {
            return nil
        }
    }
    
    func getItems() -> [T] {
        return self.items
    }
    
    func addItem(item: T) {
        self.items.append(item)
    }
    
    func addItem(item: T, atIndex index: Int) {
        if 0..<self.items.count ~= index {
            self.items.insert(item, atIndex: index)
        } else {
            self.items.append(item)
        }
    }
    
    func addItems(items: [T]) {
        self.items.appendContentsOf(items)
    }
    
    func deleteItem(atIndex index: Int){
        if 0..<self.items.count ~= index {
            self.items.removeAtIndex(index)
        }
    }
    
    func deleteAll() {
        if self.items.count != 0 {
            self.items.removeAll()
        }
    }
    
}
