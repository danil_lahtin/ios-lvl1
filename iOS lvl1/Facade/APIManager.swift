//
//  APIManager.swift
//  iOS lvl1
//
//  Created by Danil Lahtin on 21/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import UIKit


// Class is a facade singleton.
// Do work with data and internet
class APIManager: NSObject {
    
    // MARK: - Fields
    static let sharedInstance = APIManager()
    
    let dataManager = DataManager<App>()
    let httpClient = HTTPClient()
    
    // MARK: - DataManager work
    
    func getApp(atIndex index: Int) -> App? {
        return self.dataManager.getItem(atIndex: index)
    }
    
    func getApps() -> [App] {
        return self.dataManager.getItems()
    }
    
    func addApp(app: App) {
        self.dataManager.addItem(app)
    }
    
    func addApp(app: App, atIndex index: Int) {
        self.dataManager.addItem(app, atIndex: index)
    }
    
    func addApps(apps: [App]) {
        self.dataManager.addItems(apps)
    }
    
    func deleteApp(atIndex index: Int) {
        self.dataManager.deleteItem(atIndex: index)
    }
    
    func deleteAll() {
        self.dataManager.deleteAll()
    }
    
    // MARK: - HTTPClient work
    
    func fetchApps(completionHandler:(Bool) -> ()){
        let request = self.httpClient.createURLRequest()
        self.httpClient.getJSON(request){(jsonData, error) in
            if error != nil {
                print(error?.localizedDescription)
                completionHandler(false)
                return
            }
            if let dict = jsonData as? [[String:AnyObject]]{
                var apps = [App]()
                for item in dict{
                    let app = App()
                    app.setValuesForKeysWithDictionary(item)
                    apps.append(app)
                }
                self.dataManager.deleteAll()
                self.dataManager.addItems(apps)
                completionHandler(true)
                
            } else {
                print("Wrong JSON")
                completionHandler(false)
                return
            }
            
        }
    }
    
    func downloadImage(fromURL urlString: String, completionHandler: (UIImage?, Bool) -> ()){
        self.httpClient.getImage(withURL: urlString) {(image, success) in
            if success != nil{
                completionHandler(image, success!)
            } else {
                completionHandler(image, false)
            }
        }
    }
    
}
