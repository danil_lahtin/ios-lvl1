//
//  ListTableViewController.swift
//  iOS lvl1
//
//  Created by Danil Lahtin on 21/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import UIKit

class ListTableViewController: UITableViewController {

    var apps = [App]()
    
    // MARK: - UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // fetch for Apps and set apps array if succeded
        APIManager.sharedInstance.fetchApps(){(success) in
            if success {
                self.apps = APIManager.sharedInstance.getApps()
                dispatch_async(dispatch_get_main_queue()){
                    self.tableView.reloadData()
                }
            }
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        AppDefaults.mainTabBarController?.navigationItem.rightBarButtonItems = self.navigationItem.rightBarButtonItems
    }
    
    override func viewDidDisappear(animated: Bool) {
        AppDefaults.mainTabBarController?.navigationItem.rightBarButtonItems = nil
    }
    
    // MARK: - UITableViewDataSource

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.apps.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! ListTableViewCell

        cell.app = self.apps[indexPath.row]

        return cell
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            APIManager.sharedInstance.deleteApp(atIndex: indexPath.row)
            //self.apps = APIManager.sharedInstance.getApps()
            self.apps.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

    @IBAction func moreActionsButtonTapped(sender: AnyObject) {
        if #available(iOS 8.0, *) {
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
            
            let deleteFirst10Action = UIAlertAction(title: "Delete first 10 items", style: .Default){(action) in
                for _ in 0...9 {
                    APIManager.sharedInstance.deleteApp(atIndex: 0)
                }
                self.apps = APIManager.sharedInstance.getApps()
                dispatch_async(dispatch_get_main_queue()){
                    self.tableView.reloadData()
                }
            }
            
            let reloadDataAction = UIAlertAction(title: "Reload data", style: .Default){ (action) in
                APIManager.sharedInstance.fetchApps({ (success) in
                    if success {
                        self.apps = APIManager.sharedInstance.getApps()
                        dispatch_async(dispatch_get_main_queue()){
                            self.tableView.reloadData()
                        }
                    }
                })
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            
            
            actionSheet.addAction(deleteFirst10Action)
            actionSheet.addAction(reloadDataAction)
            actionSheet.addAction(cancelAction)
            
            self.presentViewController(
                actionSheet,
                animated: true,
                completion: nil
            )

        } else {
            // Fallback on earlier versions
        }
        
    }
}
