//
//  MainTabBarController.swift
//  iOS lvl1
//
//  Created by Danil Lahtin on 21/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController, MenuTableViewControllerDelegate {

    var maskView = UIView()
    var menuView = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("MenuView") as! MenuTableViewController
    var movingLeftConstraint: NSLayoutConstraint?
    var toggled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDefaults.mainTabBarController = self
        
        // MARK: - Menu preparing
        for (index,item) in self.tabBar.items!.enumerate() {
            Menu.addMenuItem(withName: item.title!, andPage: index)
        }
        
        
        // Adjust maskView
        self.maskView.backgroundColor = AppDefaults.defaultMaskColor
        self.maskView.frame = self.view.frame
        self.maskView.alpha = 0
        self.maskView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(toggleMenu as Void -> Void)))
        self.view.addSubview(self.maskView)
        
        // Set menuView
        self.menuView.delegate = self
        self.addChildViewController(self.menuView)
        self.view.addSubview(self.menuView.view)
        self.menuView.didMoveToParentViewController(self)
        self.menuView.view.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraint = NSLayoutConstraint(
            item: self.menuView.view,
            attribute: .Top,
            relatedBy: .Equal,
            toItem: self.topLayoutGuide,
            attribute: .Bottom,
            multiplier: 1,
            constant: 0
        )
        
        let bottomConstraint = NSLayoutConstraint(
            item: self.menuView.view,
            attribute: .Bottom,
            relatedBy: .Equal,
            toItem: self.view,
            attribute: .Bottom,
            multiplier: 1,
            constant: 0
        )
        
        let widthConstraint = NSLayoutConstraint(
            item: self.menuView.view,
            attribute: .Width,
            relatedBy: .Equal,
            toItem: nil,
            attribute: .NotAnAttribute,
            multiplier: 1,
            constant: self.view.frame.size.width * AppDefaults.defaultMenuWidth
        )
        
        self.movingLeftConstraint = NSLayoutConstraint(
            item: self.menuView.view,
            attribute: .Left,
            relatedBy: .Equal,
            toItem: self.view,
            attribute: .Left,
            multiplier: 1,
            constant: -widthConstraint.constant
        )
        
        self.view.addConstraints([topConstraint, bottomConstraint, widthConstraint, self.movingLeftConstraint!])
        
    }
    
    
    func didSelectMenuItem(atIndexPath indexPath: NSIndexPath) {
        self.toggleMenu()
        if indexPath.row == self.selectedIndex {
            return
        }
        let fromView = selectedViewController!.view
        let toView = self.viewControllers![indexPath.row].view
        UIView.transitionFromView(
            fromView,
            toView: toView,
            duration: AppDefaults.defaultAnimationTime / 2,
            options: UIViewAnimationOptions.TransitionCrossDissolve) { (finished) in
                self.selectedIndex = indexPath.row
        }
        
        
    }
    
    
    @IBAction func menuButtonTapped(sender: AnyObject) {
        self.toggleMenu()
    }
    
    func toggleMenu(){
        if self.toggled {
            self.hideMenu(nil)
        } else {
            self.showMenu(nil)
        }
    }
    
    
    func showMenu(completionHandler: ((Bool) -> Void)? = nil) {
        self.toggled = true
        UIView.animateWithDuration(
            AppDefaults.defaultAnimationTime,
            animations: {() in
                self.maskView.alpha = 1
                self.movingLeftConstraint!.constant = 0
                self.view.layoutIfNeeded()
            },
            completion: completionHandler)
    }
    
    func hideMenu(completionHandler: ((Bool) -> Void)? = nil) {
        self.toggled = false
        UIView.animateWithDuration(
            AppDefaults.defaultAnimationTime,
            animations: {() in
                self.maskView.alpha = 0
                self.movingLeftConstraint!.constant = -self.menuView.view.frame.size.width
                self.view.layoutIfNeeded()
            },
            completion: completionHandler)
    }
}
