//
//  Extensions.swift
//  iOS lvl1
//
//  Created by Danil Lahtin on 22/07/16.
//  Copyright © 2016 Danil Lahtin. All rights reserved.
//

import Foundation

extension Double{
    
    func timeIntervalToString(usingFormat format: String = AppDefaults.defaultDateFormat) -> String{
        let formatter = NSDateFormatter()
        formatter.dateFormat = format
        let date = NSDate(timeIntervalSince1970: self)
        return formatter.stringFromDate(date)
    }
}